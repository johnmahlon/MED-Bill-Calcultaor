This is a Python script that parses a CSV file and calculates how much money has been spent on electricity.

To run this:
```bash
python elecCalc.py SomeCSVFile.csv
```
